
import java.util.*;
import java.io.*;

public class CommandLineInterface {
    Scanner userInput = new Scanner(System.in);
    FileCopyMethods copyMethods = new FileCopyMethods();
    ArrayList<File> sourceFileArray = new ArrayList<File>();// used to store files
    ArrayList<String> sourceFileNameArray = new ArrayList<String>(); // used to store the directory strings and names of files

    public static void main(String[] args) {
        new CommandLineInterface();
    }

    public CommandLineInterface() throws InputMismatchException {
        while(true) {
            System.out.println("choose from one of the options below");
            System.out.println();
            System.out.println("1: Copy Files\n2: Copy Directory\n3: Copy Directory Tree\n4: Exit");

            int mainMenuChoice = userInput.nextInt();
            switch(mainMenuChoice) {
                case 1:
                    System.out.println("Enter absolute path for source file");
                    String sourceDirectoryName = userInput.next();
                    //use defines path for source directory

                    System.out.println("Enter the absolute file path where the files will be copied to");
                    String destinationDirectoryName = userInput.next();
                    //user defines path for destination directory


                    populateSourceFileArray(sourceDirectoryName,destinationDirectoryName);
                    //method allows user to select multiple files from sourceDirectory and add them to an arrayList

                    try {
                        copyFiles(destinationDirectoryName);
                        //method copies all selected files in the sourceFileArray over to the destination directory
                    } catch(IOException ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 2:
                    System.out.println("Enter absolute path for source file");
                    String srcDirectoryName = userInput.next();
                    //use defines path for source directory

                    System.out.println("Enter the absolute file path where the files will be copied to");
                    String destDirectoryName = userInput.next();
                    //user defines path for destination directory
                    try {
                        copyMethods.copyDirectory(srcDirectoryName,destDirectoryName);
                    } catch(IOException ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 3:
                    System.out.println("Source name");
                    String sourceName = userInput.next();

                    System.out.println("Destination name");
                    String destinationName = userInput.next();
                    try {
                        copyMethods.copyTree(sourceName,destinationName);
                        System.out.println("The process has finished.");
                    } catch(IOException ex) {
                        ex.printStackTrace();
                    }
                    break;
                case 4:
                    System.exit(0);
                    break;
            }
        }
    }

    public void populateSourceFileArray(String sourceDirectoryName,String destinationDirectoryName) {
        boolean fileArrayFull = false;
        while(!fileArrayFull) {
            //deepCopyMethods.printFiles(sourceDirectoryName);
            //print out all files from sourceDirectory for user to choose from
            System.out.println("Enter the name of the files to copy. Press 'e' when you are finished.");


            //user choose from multiple files. Files are placed into sourceFileArray
            String sourceFileName = userInput.next();
            File currentFile = new File(sourceDirectoryName + sourceFileName);

            if(sourceFileName.equalsIgnoreCase("e")) {
                fileArrayFull = true;
                //end while loop

            } else if((currentFile.isFile()) && (currentFile.exists())) {
                //check if the file is a file and exists, if so add the file to the source file array to later be copied
                sourceFileNameArray.add(sourceFileName);
                //file name array stores the names of each file. Later used to name the destination files
                sourceFileArray.add(currentFile);
                System.out.println("File added to array");

            } else if((!currentFile.isFile()) || (!currentFile.exists())) {
                System.out.println("The file you have entered either does not exist, or it is a directory. Try again.");
                populateSourceFileArray(sourceDirectoryName,destinationDirectoryName);
                //recursive call to method in case the file does not exist, or is a directory
            }
        }
    }

    public void copyFiles(String destinationDirectoryName) throws IOException {
        int totalPercent = sourceFileArray.size();
        int currentPercent = 0;
        System.out.println("Working");

        for(int i = 0; i < sourceFileArray.size(); i++) {
            File destinationFile = new File(destinationDirectoryName + sourceFileNameArray.get(i));
            copyMethods.copy(sourceFileArray.get(i),destinationFile);
            copyMethods.workStatus(totalPercent,currentPercent);

            if(i < sourceFileArray.size()) {
                currentPercent++;
            } else if(i == sourceFileArray.size()) {
                System.out.println("100%");
                System.out.println("Work Finished");
            }
        }
    }
}

