import java.util.*;
import java.io.*;

public class FileCopyMethods {
    //this class holds the core methods of the program

    public void copyTree(String sourceDirectoryName, String destinationDirectoryName) throws IOException {
        File sourceDirectory = new File(sourceDirectoryName); //creates a file for the source directory
        String sourceDirectoryList[] = sourceDirectory.list(); //list all files in source directory
        File destinationDirectory = new File(destinationDirectoryName);


        if(!sourceDirectory.exists()) {
            System.out.println("The source directory does not exist. Back to the drawing board.");
        } else if(sourceDirectory.isDirectory()) {
            if(!destinationDirectory.exists()) {
                destinationDirectory.mkdir();
            }

            for (String currentFile : sourceDirectoryList) {
                File sourceFile = new File(sourceDirectory, currentFile);
                File destinationFile = new File(destinationDirectory, currentFile);
                System.out.println("Created new directory: " + destinationFile.toString());
                copyTree(sourceFile.toString(),destinationFile.toString());
            }
        } else {
            copy(sourceDirectory,destinationDirectory);
        }
    }

    public void copy(File sourceFile, File destinationFile) throws IOException {
        /*
        method copies a single file from the source directory to the
        destination directory
         */
        if(!sourceFile.isHidden()) {
            //check for file permissions

            DataInputStream inputStream =
                    new DataInputStream(new BufferedInputStream(new FileInputStream(sourceFile)));
            DataOutputStream outputStream =
                    new DataOutputStream(new BufferedOutputStream(new FileOutputStream(destinationFile)));

            int position = 0;

            //copy data using DataInputStreams
            while((position = inputStream.read()) != -1) {
                outputStream.write((byte)position);
            }

            //close the streams
            inputStream.close();
            outputStream.close();

            //verify the file was copied
            System.out.println("Copied file: " + sourceFile.toString());
        }
    }

    public void copyDirectory(String sourceDirectoryName, String destinationDirectoryName) throws IOException {
        File sourceFile = new File(sourceDirectoryName);
        File[] sourceDirectoryFileList = sourceFile.listFiles();
        int totalPercent = sourceDirectoryFileList.length;
        int currentPercent = 0;
        System.out.println("Working.");

        for(int n = 0; n < sourceDirectoryFileList.length; n++) {
            String sourceFileName = sourceDirectoryFileList[n].getName();
            File directoryFile = new File(destinationDirectoryName + sourceFileName);

            if(sourceDirectoryFileList[n].isFile()) {
                copy(sourceDirectoryFileList[n],directoryFile);
            }
            workStatus(totalPercent,currentPercent);
            currentPercent++;
        }
    }


    public void workStatus(int totalPercent, int currentPercent) {
        int status = currentPercent * 100 / totalPercent;
        System.out.println(status + "%");
    }
}


