
Exodus Version 1.0
==========
Last date edited: 07/15/2014

kfranco@hmamail.com


Exodus is a file copying utility. This is the command line interface version.
So far this software has only been tested under small file systems (hard drives no larger than 500Gb).
There is no guarantee that this software will work as intended. Use at your own risk.

Installation Instructions:
The program is contained within a single
.jar file. This type of file can be run from any computer (Windows, MacOS, Linux) that has Java installed. To run this software open up a terminal (or command prompt if using Windows), navigate to where the .jar file is located and then enter "java -jar Exodus.jar" 

If you do not know how to use your operating systems command line then you should spend time reading a few tutorials and getting familiar with it, as it is necessary to use this program. 

The interface for this  software is pretty straight-forward;
1. wait for the prompt
2. choose the relevant option shown

If option #1:
Enter the source file as shown here "/home/UserName/Pictures/chosenPicture"
When you are finished selecting files from the directory type "e" to let the 
program know that you are finished.
then enter the destination directory as shown here "/home/UserName/chosenDirectory/"

If Option #2:
This option copies an entire directory. Only the files within the chosen directory will be copied, not 
any subdirectories.
Enter the source file as shown "/home/UserName/Pictures/"
then enter the destination directory as shown "/home/UserName/newPictureDirectory/"

If option #3:
All files and subdirectories under the chosen source directory will be copied to the destination directory.
Enter the source as shown here "/home/UserName/Pictures/"
then enter the destination directory as shown here "/home/UserName/newPictureDirectory/"
